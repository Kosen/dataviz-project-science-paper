// data variables
int totalLines = 0;
String[] line;
ArrayList<singlePaper> allPapers = new ArrayList();
int maxDownloads = 0;
int maxCitations = 0;
int maxKeywords = 0;
int maxAuthors = 0;
int maxAuthorsDistance = 0;
int maxKeywordsDistance = 0;
HashMap<String, ArrayList> authorMap;
HashMap<String, ArrayList> keywordMap;

float xPerc;
float yPerc;
int back = 800;
int plaice = 80;
int chartSize = back-plaice*2;
int chartSizeOffset = 20;

//ArrayList fields = new ArrayList();
//ArrayList activeFields = new ArrayList();

float originX = plaice/2;
float originY = chartSize+1.5*plaice ;
float endX = originX + (chartSize + plaice/2) ;
float endY = originY - (chartSize + plaice/2) ;
float chartAbsoluteX = plaice;
float ChartAbsoluteY = chartSize + plaice;

int itemPlotSize = 10;
int itemPlotSelectedSize = 20;

void setup(){
    readData();
    //back = 800;
    size(back,back);
    background (200);
    smooth();
    // importData_Remi();
}

//read files in and create paper objects in papers array
//read files, create paper array to store them all
////////////////////////////////
/*void importData_Remi() {
  String[] data = loadStrings("fakeDat.csv");
  for (int i=0; i < data.length; i++){  //Start from zero, not one
  String[] temp=split(data[i],",");
    fields.add(new field(temp[0],float(temp[1]),float(temp[2]),float(temp[3]),float(temp[4])));
    /*if(maxDownloads < float(temp[1])){ maxDownloads = float(temp[1]); } //Setting the maximum value of download
    if(maxCitations < float(temp[2])){ maxCitations = float(temp[2]); } //Setting the maximum value of citation
    if(maxKeywordsDistance < float(temp[4])){ maxKeywordsDistance = float(temp[4]); } //Setting the maximum value of distance
  }
  println("maxKeywordsDistance :" + maxKeywordsDistance);
}*/
void readData() {
    int numFiles = 2;
    int papersLoaded = 0;
    for (int i = 0; i < numFiles; i++ ) {
        // Construct filename
        String leadingZero = "";
        if ( i < 10 ) {
          leadingZero = "0";
        }
        String filename = "data-files/CHI" + leadingZero + i + ".csv";
        String[] readLines = loadStrings(filename);
        int fileLines = readLines.length - 1;
        totalLines = totalLines + fileLines;

        for (int j = 1; j < fileLines + 1; j++ ) {
            line = split(readLines[j], "\t");

            // Create paper object and load it
            String[] elements;
            int distanceToPaper_index = -1;

            int citation_number = Integer.valueOf(line[4].replace(".", ""));
            if ( citation_number > maxCitations ) {
                maxCitations = citation_number;
            }

            int publication_download = Integer.valueOf(line[7].replace(".", ""));
            if ( publication_download > maxDownloads ) {
                maxDownloads = publication_download;
            }

            elements = split(line[8], ';');
            ArrayList<String> keywords = new ArrayList();
            for ( int k = 0, len = elements.length; k < len; k++ ) {
                if ( elements[k].length() > 3 ) {
                    keywords.add(elements[k]);
                    // println("added keyword: '"+elements[k]+"'");
                }
            }
            if ( elements.length > maxKeywords) {
                maxKeywords = elements.length;
            }

            elements = split(line[10], ';');
            ArrayList<String> authors = new ArrayList();
            for ( int k = 0, len = elements.length; k < len; k++ ) {
                if ( elements[k].length() > 3 ) {
                    authors.add(elements[k]);
                    // println("added author: '"+elements[k]+"'");
                }
            }
            if ( elements.length > maxAuthors) {
                maxAuthors = elements.length;
            }

            int year_publication = Integer.valueOf("20"+leadingZero+i);

            int paper_id = Integer.valueOf("20"+leadingZero+i+"0"+j);
            String title_short = line[3];
            String title_long = line[3];
            singlePaper paper = new singlePaper(citation_number, publication_download, distanceToPaper_index, keywords, paper_id, title_short, title_long, year_publication, authors);
            allPapers.add(paper);
            papersLoaded++;
            println("Papers loaded: "+papersLoaded);
        }
    }

    // Build keyword and authors arrays
    // Each lists each author/keyword with a list of papers containing it
    println("building HashMaps");
    keywordMap = new HashMap<String, ArrayList>();
    authorMap = new HashMap<String, ArrayList>();
    for (singlePaper paper : allPapers) {
        ArrayList<String> authors = (ArrayList) paper.getAuthors();
        for (String author : authors) {
            ArrayList<singlePaper> author_papers = (ArrayList) authorMap.get(author);
            if ( author_papers == null ) {
                author_papers = new ArrayList<singlePaper>();
            } else {
                // println("author "+author+" found authors: "+author_papers.size());
            }
            author_papers.add(paper);
            authorMap.put(author, author_papers);
        }

        ArrayList<String> keywords = paper.getKeywords();
        for (String keyword : keywords) {
            ArrayList<singlePaper> keyword_papers = (ArrayList) keywordMap.get(keyword);
            if ( keyword_papers == null ) {
                keyword_papers = new ArrayList<singlePaper>();
            } else {
                // println("keyword "+keyword+" found keywords: "+keyword_papers.size());
            }
            keyword_papers.add(paper);
            keywordMap.put(keyword, keyword_papers);
        }
    }

    println("total papers: "+totalLines);
    println("allPapers: "+allPapers.size() );
    println("max authors: "+maxAuthors);
    println("max keywords: "+maxKeywords);
    println("max downloads: "+maxDownloads);
    println("max citations: "+maxCitations);
    println("unique keywords: "+keywordMap.keySet().size() );
    println("unique authors: "+authorMap.keySet().size() );

    // // Heavy Test: compare all papers against all other papers for keyword matches
    // int keyword_matches_found = 0;
    // int author_matches_found = 0;
    // singlePaper prevPaper = null;
    // for (singlePaper paper1 : allPapers) {
    //     for (singlePaper paper2 : allPapers) {
    //         if ( paper1 != paper2 ) {
    //             int author_distance = calculateAuthorDistance(paper1, paper2);
    //             println("author distance for paper1&2: "+author_distance);
    //             author_matches_found = author_matches_found + author_distance;
    //             int keyword_distance = calculateKeywordDistance(paper1, paper2);
    //             println("keyword distance for paper1&2: "+keyword_distance);
    //             keyword_matches_found = keyword_matches_found + keyword_distance;
    //             // println("author_matches_found: "+author_matches_found);
    //             // println("keyword_matches_found: "+keyword_matches_found);
    //         }
    //     }
    // }
    // println("total authors: " + author_matches_found);
    // println("total keywords: " + keyword_matches_found);


    // Convert ints to floats for vis code
    // maxKeywordsDistance = maxAuthorsDistance;
}




// Distance is calculated by taking two papers and seeing how many keywords they share in common
int calculateKeywordDistance(singlePaper firstPaper, singlePaper secondPaper) {
    // for each keyword in one paper
    //  check to see if the other paper is that keyword's paper list
    int numMatches = 0;
    ArrayList<String> keywords_in_first_paper = firstPaper.getKeywords();
    ArrayList<String> keywords_in_second_paper = secondPaper.getKeywords();
    // println(" authors in 1st paper: "+authors_in_first_paper.size()+"  "+authors_in_first_paper);
    // println(" authors in 2nd paper: "+authors_in_second_paper.size()+"  "+authors_in_second_paper);
    for (String a_keyword_in_first_paper : keywords_in_first_paper) {
        ArrayList<singlePaper> papers_by_the_keyword_in_first_paper = (ArrayList) keywordMap.get(a_keyword_in_first_paper);
        if ( keywords_in_second_paper.contains(a_keyword_in_first_paper) ) {
            numMatches++;
            println("match found, paper2 contains "+a_keyword_in_first_paper);
        } else {
            // println("no match found. paper2 doesn't contain "+a_keyword_in_first_paper);
        }
    }
    if ( numMatches > maxKeywordsDistance ) { maxKeywordsDistance = numMatches; }
    return numMatches;
}

// Distance is calculated by taking two papers and seeing how many authors they share in common
int calculateAuthorDistance(singlePaper firstPaper, singlePaper secondPaper) {
    println("calc auth distance:");
    // for each author in one paper
    //  check to see if the other paper is that author's paper list
    int numMatches = 0;
    ArrayList<String> authors_in_first_paper = firstPaper.getAuthors();
    ArrayList<String> authors_in_second_paper = secondPaper.getAuthors();
    // println(" authors in 1st paper: "+authors_in_first_paper.size()+"  "+authors_in_first_paper);
    // println(" authors in 2nd paper: "+authors_in_second_paper.size()+"  "+authors_in_second_paper);
    for (String an_author_in_first_paper : authors_in_first_paper) {
        ArrayList<singlePaper> papers_by_the_author_in_first_paper = (ArrayList) authorMap.get(an_author_in_first_paper);
        if ( authors_in_second_paper.contains(an_author_in_first_paper) ) {
            numMatches++;
            println("match found, paper2 contains "+an_author_in_first_paper);
        } else {
            // println("no match found. paper2 doesn't contain "+an_author_in_first_paper);
        }
    }
    if ( numMatches > maxAuthorsDistance ) { maxAuthorsDistance = numMatches; }
    return numMatches;
}



public class singlePaper {

    //-- Core
    private int citation_number;
    private int publication_download;
    private int distanceToPaper_index;
    private ArrayList<String> keywords;
    private int paper_id;

    //--Visualisation
    private String title_short;

    //-- Information displayed when interaction
    private String title_long;
    private int year_publication;
    private ArrayList<String> authors;

    public singlePaper(){ }

public singlePaper (int citation_number, int publication_download, int distanceToPaper_index, ArrayList<String> keywords, int paper_id, String title_short, String title_long, int year_publication, ArrayList<String> authors) {
        this.citation_number = citation_number;
        this.publication_download = publication_download;
        this.distanceToPaper_index = distanceToPaper_index;
        this.keywords = keywords;
        this.paper_id = paper_id;
        this.title_short = title_short;
        this.title_long = title_long;
        this.year_publication = year_publication;
        this.authors = authors;
    }

    public int getCitation_number() {
        return citation_number;
    }

    public void setCitation_number(int citation_number) {
        this.citation_number = citation_number;
    }

    public int getPublication_download() {
        return publication_download;
    }

    public void setPublication_download(int publication_download) {
        this.publication_download = publication_download;
    }

    public int getDistanceToPaper_index() {
        return distanceToPaper_index;
    }

    public void setDistanceToPaper_index(int distanceToPaper_index) {
        this.distanceToPaper_index = distanceToPaper_index;
    }

    public ArrayList<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(ArrayList<String> keywords) {
        this.keywords = keywords;
    }

    public int getpaper_id() {
        return paper_id;
    }

    public void setpaper_id(int paper_id) {
        this.paper_id = paper_id;
    }

    public String gettitle_short() {
        return title_short;
    }

    public void settitle_short(String title_short) {
        this.title_short = title_short;
    }

    public String gettitle_long() {
        return title_long;
    }

    public void settitle_long(String title_long) {
        this.title_long = title_long;
    }

    public int getYear_publication() {
        return year_publication;
    }

    public void setYear_publication(int year_publication) {
        this.year_publication = year_publication;
    }

    public ArrayList<String> getAuthors() {
        return authors;
    }

    public void setAuthors(ArrayList<String> authors) {
        this.authors = authors;
    }

}


void draw(){
  background (200);
  //noStroke();
  //plaice = 80;
  //chartSize = back-plaice*2;

  fill(255);
  stroke(0);

  /*float originX = plaice/2;
  float originY = chartSize+1.5*plaice ;
  float endX = originX + (chartSize + plaice/2) ;
  float endY = originY - (chartSize + plaice/2) ;
  float chartAbsoluteX = plaice;
  float ChartAbsoluteY = chartSize + plaice;

  int itemPlotSize = 10;
  int itemPlotSelectedSize = 20;*/


  fill(255);
  ellipse(chartAbsoluteX,ChartAbsoluteY,10,10);


  strokeWeight(2);
  stroke(0, 0, 0);
  line(originX,originY,endX,originY); //vertical axis
  line(originX,originY,originX,endY); //horizontal axis


  fill(255);
  ellipse(originX,originY,10,10);


  textSize(20);
  fill(0, 102, 153);
  textAlign(CENTER);
  text("Download number", back/2, back-10);
  writeVerticalText(30, back/2, "Citations number");
  fill(255);
  strokeWeight(0);
  rect(plaice-chartSizeOffset, plaice-chartSizeOffset, chartSize+2*chartSizeOffset, chartSize+2*chartSizeOffset);
  stroke(200);
  int length = 600 ;


  for (int i=0; i<=3; i++){
    line(plaice+(i*(chartSize/4)), plaice, plaice+(i*(chartSize/4)), plaice+chartSize);
    line(plaice, plaice+chartSize-(i*(chartSize/4)), plaice+chartSize, plaice+chartSize-(i*(chartSize/4)));
  }

  for (int i=0; i < allPapers.size(); i++){
    singlePaper f=(singlePaper)allPapers.get(i);

    /*float dotCol1 = map(f.x,0,1,0,maxDownloads);
    float dotCol2 = map(f.y,0,1,0,maxCitations);*/

   float fx = f.getPublication_download();
   float fy = f.getCitation_number();    

    float dotCol1 = map(fx,0,1,0,maxDownloads);
    float dotCol2 = map(fx,0,1,0,maxCitations);
   
    
    fill(dotCol1, dotCol2, 200);

    stroke(100);

    float relativePositionX = chartAbsoluteX + map(fx,0,maxDownloads,0,(float)chartSize);
    float relativePositionY = ChartAbsoluteY - map(fy,0,maxCitations,0,(float)chartSize);


    ellipse(relativePositionX,relativePositionY, itemPlotSize, itemPlotSize);
    fill(0);

    noFill();
    strokeWeight(1);
    stroke(204,51,0);
    //rect( relativePositionX -itemPlotSize/2 , relativePositionY-itemPlotSize/2 , itemPlotSize , itemPlotSize); // Detection zone of action

    if(mouseX > relativePositionX-itemPlotSize && mouseX < relativePositionX+itemPlotSize && mouseY > relativePositionY-itemPlotSize && mouseY < relativePositionY +itemPlotSize)
    {
      //linkToOtherNode(relativePositionX, relativePositionY, i);
      linkToOtherNode(relativePositionX, relativePositionY, f);
      fill(255,255,0);
      ellipse(relativePositionX,relativePositionY, itemPlotSelectedSize, itemPlotSelectedSize);
      fill(0);
      textSize(20);
      text (f.gettitle_short(),relativePositionX,relativePositionY + 30);
      textSize(20);
      text ("("+(int)f.getYear_publication()+")",relativePositionX,relativePositionY + 50);
    }
  }
}

void writeVerticalText(int x, int y, String text) {
 pushMatrix();
 translate(x, y);
 rotate(-HALF_PI);
 text(text, 0, 0);
 popMatrix();
}

void linkToOtherNode(float nodeX, float nodeY, singlePaper parentPaper)
{
  
  float fdist =0.0001 ;
  float  distanceWeight = 0; 
  float red = 0;
  float blue = 0;
  
  for (int i=0; i < allPapers.size(); i++){
    singlePaper f=(singlePaper)allPapers.get(i);
    float fx = f.getPublication_download();
    float fy = f.getCitation_number(); 
    //float fdist = (int)calculateKeywordDistance(parentPaper, f);
    //println("---------------------------\n 1parper :" + parentPaper.gettitle_short()  + "\n parperChild :"+ f.gettitle_short()+"\n---------------------------\n");
    
    
    
    fdist = calculateKeywordDistance(parentPaper, f);
    distanceWeight = fdist / maxKeywordsDistance;
    //println("---------------------------\n keywordDistance :" + calculateKeywordDistance(parentPaper, f)+"\n keywordDistance :" + fdist+ "\n---------------------------\n");
   
   
    float childX = chartAbsoluteX + map(fx,0,maxDownloads,0,(float)chartSize);
    float childY = ChartAbsoluteY - map(fy,0,maxCitations,0,(float)chartSize);

    
    //println("---------------------------\n maxKeywordsDistance :" + maxKeywordsDistance+"\n---------------------------\n");
    //println("distanceWeight :" + distanceWeight);
    //float  distanceFactor = map(fdist,0,maxKeywordsDistance,0,255);
    //println("distanceFactor :" + distanceFactor);
    stroke(0,0,0);//RED
    
    //stroke(255,0,0);//RED
    red = 255*(distanceWeight-1);
    //blue = 0;
    red = 255*(1*(1-distanceWeight));
    
    stroke(red,0,blue);//RGB -> GREEN : Minimal distance & RED: Maximal distance
    

    //strokeWeight(0);
    if(distanceWeight == 0)
    {
      strokeWeight(0);
    }
    else
    {
       strokeWeight(distanceWeight*10); //need to be adapted to the distance ArrayList  + the stroke weight rendering change based on the right result;
       // HSL dynamic color rendering
       //stroke(HSV2RGB(0, 100, distanceFactor)); //use the color red
    }
    line(nodeX, nodeY, childX, childY);
  }
}

// Given H,S,L in range of 0-360, 0-1, 0-1  Returns a Color
color HSV2RGB(float hue, float saturation, float value) {
    int h = (int)((hue/60) % 6);
    float f = (hue/60)-h;
    float l = value * (1 - saturation);
    float m = value * (1 - f * saturation);
    float n = value * (1 - (1 - f) * saturation);

    switch (h) {
      case 0: return color(value, n, l);
      case 1: return color(m, value, l);
      case 2: return color(l, value, n);
      case 3: return color(l, m, value);
      case 4: return color(n, l, value);
      case 5: return color(value, l, m);
      default: throw new RuntimeException("Something went wrong when converting from HSV to RGB. Input was " + hue + ", " + saturation + ", " + value);
    }
}

