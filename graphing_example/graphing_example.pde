// data variables
int nbOfCountries = 0;
float[] x;
float[] y;
String[][] data;
String[] lines;
String[] line;
String[] headers;

boolean[][] ValidNum;

int col1=1;
int col2=2;

float maxX;
float maxY;

// drawing variables
int height  = 800;
int width = 1000;



////////////////////////////////
void setup(){
  size(width, height);
  readData();
//  drawpoint();
//  noLoop();
}



////////////////////////////////
void readData() {
  lines = loadStrings("countries.csv");
  nbOfCountries = lines.length - 1;
  headers = split(lines[0], ";");

println("nb countries: "+nbOfCountries);
println("nb headers: "+headers.length);

  // init arrays
  data = new String[nbOfCountries][headers.length];
  ValidNum = new boolean[nbOfCountries][headers.length];
  x = new float[nbOfCountries];
  y = new float[nbOfCountries];

  // load data into array
  for(int i=0; i < nbOfCountries; i++) {

    // populate the data array
    data[i] = split(lines[i+1],";");

    // populate the NaN array
    for(int j=0; j < headers.length; j++) {
      if ( data[i][j].equals("X") ) {
        ValidNum[i][j] = false;
      } else {
        ValidNum[i][j] = true;
      }

//println("i:"+i+ " j:"+j+ " value:"+ data[i][j]);
//println("ValidNum:"+ ValidNum[i][j]);
    }
  }

  // pull out two columns into x and y and get the max values
  maxX = 0;
  maxY = 0;
  for(int i=1; i < nbOfCountries; i++) {
    x[i] = float(data[i][col1]);
    y[i] = float(data[i][col2]);
    if ( (ValidNum[i][col1] == true) && (x[i] > maxX) ) {
      maxX = x[i];
    }
    if ( (ValidNum[i][col2] == true) && (y[i] > maxY) ) {
      maxY = y[i];
    }
  }

  for(int i=1; i < nbOfCountries; i++) {
    x[i] = mapX(x[i]);
    y[i] = mapY(y[i]);
  }

//println(headers);  
}




/////////////////////////////////////
// drawing global variables
int originX = 100;
int originY = 100;
int endX = 700;
int endY = 700;

void draw(){
  background(255);
//  ellipse(10,10,10,10);

  // draw lines
  strokeWeight(2);
  stroke(0,0,0);
  int length = 600;
  // y-axis
  line(originX,originY,originX,endY);
  // x-axis
  line(originX,endY,endX,endY);

  // label axis
  textSize(32);
  fill(0,102,153);
  textAlign(CENTER);
  text("X-axis : Label for the axis", width/2, height-10);
  writeVerticalText(30, height/2, "Y-axis : Label for the axis");

  drawpoint();

text("move",mouseX,mouseY);

}



void writeVerticalText(int x, int y, String text){
  pushMatrix();
  translate(x,y);
  rotate(-HALF_PI);
  text(text, 0 ,0);
  popMatrix();
}

float mapX(float x){
println("x: "+x);
  return map(x, 0, maxX, originX, endX);
}
float mapY(float y){
println("y: "+y);
float a=map(y, 0, maxY, originY, endY);
println("mapped y: "+a);
  return map(y, 0, maxY, originY, endY);
}


void drawpoint() {
  stroke(0);
  fill(175);
  int radius = 5;
  for (int i = 0; i < nbOfCountries; i++) {
    if ( ValidNum[i][col1] && ValidNum[i][col2] ) {
      ellipse(x[i], y[i], radius, radius);
      println("drawing point "+x[i]+","+y[i]);
e    }
//println("x NaN?"+ValidNum[i][col1]+" val:"+float(data[i][col1]));
//println("y NaN?"+ValidNum[i][col2]+" val:"+float(data[i][col2]));
  }
}



